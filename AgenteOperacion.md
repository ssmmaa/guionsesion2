# `AgenteOperacion`

El objetivo de este agente es recibir mensajes `ACLMessage.INFORM` que intentará transformar a un objeto `Punto2D`. Si lo consigue realizará una operación aleatoria, de entre las conocidas, y enviará un mensaje a un `AgenteConsola` con el resultado. Para ello se suscribe al servicio de páginas amarillas para conocer los `AgentesConsola` presentes en la plataforma.

La definición de las variables de instancia del agente será:

- Una lista para almacenar los `AgenteConsola` conocidos.
- Una lista para almacenar los mensajes que tenemos que enviar a un `AgenteConsola`.
- Una lista de `Punto2D` para completar una operación.

El diseño de los métodos y tareas del agente será:

### Método `setup()`

Inicializamos las variables de instancia, registramos el agente en el servicio de páginas amarillas de forma similar a lo que hemos hecho con el `AgenteConsola` pero con sus parámetros de configuración presentes en la interface `Constantes`. Para finalizar añadimos las tareas principales del agente y la tarea de suscripción al servicio de páginas amarillas para recibir notificaciones del servicio con los `AgenteConsola` conocidos.

```java
...
//Añadir las tareas principales
addBehaviour(new TareaRecepcionOperacion());
addBehaviour(new TareaEnvioConsola(this,REPETICION));
       
//Suscripción al servicio de páginas amarillas
//Para localiar a los agentes operación y consola
DFAgentDescription template = new DFAgentDescription();
ServiceDescription templateSd = new ServiceDescription();
templateSd.setType(TIPO_SERVICIO);
templateSd.setName(CONSOLA.name());
template.addServices(templateSd);
addBehaviour(new TareaSuscripcionDF(this,template));
...
```

### Método `takeDown()`

Este método solo tiene que eliminar el registro en el servicio de páginas amarillas del agente. No tiene ningún recurso asociado.

### Métodos auxiliares

Solo necesitamos un método para facilitar la legibilidad del código del agente. En nuestro caso el método que realizará la operación con el `Punto2D` y devuelve un `String` que utilizaremos para componer el mensaje para el `AgenteConsola`.

```java
    //Métodos de trabajo del agente
    private String operacion (Punto2D punto) {
        double resultado;
        
        //Se realiza una operación elegida de forma aleatoria

        Operacion opSeleccionada = Operacion.getOperacion(); 
        switch ( opSeleccionada.ordinal() ) {
            case 0:
                //Suma
                resultado = punto.getX() + punto.getY();
                return "Se ha realizado la suma de " + punto
                        + "\ncon el resultado: "+ resultado;
            case 1:
                //Resta
                resultado = punto.getX() - punto.getY();
                return "Se ha realizado la resta de " + punto
                        + "\ncon el resultado: "+ resultado;
            case 2:
                //Multiplicación
                resultado = punto.getX() * punto.getY();
                return "Se ha realizado la multiplicación de " + punto
                        + "\ncon el resultado: "+ resultado;
            default:
                //División
                if( punto.getY() != 0 ) {
                    resultado = punto.getX() / punto.getY();
                    return "Se ha realizado la división de " + punto
                        + "\ncon el resultado: "+ resultado;
                } else
                    return "OPERACION NO DEFINIDA";
        }
        
    }
```

## Clase `TareaRecepcionOperacion`

Esta será una tarea cíclica que recogerá un mensaje `INFORM` y supondremos que tiene como contenido un dos números separados por una coma que nos permite componer un objeto `Punto2D` y añadir una tarea que permita realizar una operación aleatoria entre las conocidas. Si no es así presentamos un mensaje en la consola por motivos de depuración.

```java
    public class TareaRecepcionOperacion extends CyclicBehaviour {

        @Override
        public void action() {
            //Recepción de la información para realizar la operación
            MessageTemplate plantilla = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage mensaje = myAgent.receive(plantilla);
            if (mensaje != null) {
                //procesamos el mensaje
                String[] contenido = mensaje.getContent().split(",");
                
                Punto2D punto = new Punto2D();
                try {
                    punto.setX(Double.parseDouble(contenido[PRIMERO]));
                    punto.setY(Double.parseDouble(contenido[SEGUNDO]));
                
                    operacionesPendientes.add(punto);
                
                    addBehaviour(new TareaRealizarOperacion());
                } catch (NumberFormatException ex) {
                    // No sabemos tratar el mensaje y los presentamos por consola
                    System.out.println("El agente: " + myAgent.getName() + 
                            " no entiende el contenido del mensaje: \n\t"
                            + mensaje.getContent() + " enviado por: \n\t" +
                            mensaje.getSender());
                }
            } 
            else
                block();
            
        }
    }
```

## Clase `TareaRealizarOperacion`

Esta tarea se realiza una única vez para un `Punto2D` y realiza una operación aleatoria y compondrá el mensaje que será enviado posteriormente a un `AgenteConsola`.

```java
    public class TareaRealizarOperacion extends OneShotBehaviour {

        @Override
        public void action() {
            //Realizar una operacion pendiente y añadir el mensaje
            //para la consola
            
            Punto2D punto = operacionesPendientes.remove(PRIMERO);
            
            mensajesPendientes.add(operacion(punto));
        }
        
    }
```

## Clase `TareaEnvioConsola`

Esta es una tarea que se activará cíclicamente pasado un tiempo `REPETICION` y enviará un mensaje a un `AgenteConsola`. Si cuando activa no hay mensajes no hace nada.

```java
    public class TareaEnvioConsola extends TickerBehaviour {
        //Tarea de ejemplo que se repite cada 10 segundos
        public TareaEnvioConsola(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            ACLMessage mensaje;
            if (!agentesConsola.isEmpty()) {
                if (!mensajesPendientes.isEmpty()) {
                    mensaje = new ACLMessage(ACLMessage.INFORM);
                    mensaje.setSender(myAgent.getAID());
                    mensaje.addReceiver(agentesConsola.get(PRIMERO));
                    mensaje.setContent(mensajesPendientes.remove(PRIMERO));
            
                    myAgent.send(mensaje);
                }
                else {
                    //Acciones que queremos hacer si no tenemos
                    //mensajes pendientes
                }
            }
        } 
    }
```

## Clase `TareaSuscripcionDF`

Esta es la tarea que permite suscribirnos al servicio de páginas amarillas para que nos informe de los `AgentesConsola` que se registran y cuando abandonan el registro. Los añadiremos o eliminaremos de la lista de agentes conocidos. Por motivos de depuración, presentamos un mensaje en la consola para comprobar los `AgentesConsola` conocidos.

```java
    public class TareaSuscripcionDF extends DFSubscriber {

        public TareaSuscripcionDF(Agent a, DFAgentDescription template) {
            super(a, template);
        }

        @Override
        public void onRegister(DFAgentDescription dfad) {
            Iterator it = dfad.getAllServices();
            while( it.hasNext() ) {
                ServiceDescription sd = (ServiceDescription) it.next();
                if( sd.getName().equals(CONSOLA.name()) )
                    agentesConsola.add(dfad.getName());
            }
            
            System.out.println("El agente: " + myAgent.getName() + 
                        " ha encontrado a:\n\t" + agentesConsola);
        }

        @Override
        public void onDeregister(DFAgentDescription dfad) {
            AID agente = dfad.getName();
            
            agentesConsola.remove(agente);
            
            System.out.println("El agente: " + agente.getName() + 
                                " ha sido eliminado de la lista de " +
                                myAgent.getName());        
        }
        
    }
```


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTIyMzM5OTg2MV19
-->