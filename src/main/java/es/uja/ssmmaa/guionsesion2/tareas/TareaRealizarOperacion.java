/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.Constantes;
import es.uja.ssmmaa.guionsesion2.Constantes.Operacion;
import es.uja.ssmmaa.guionsesion2.util.Punto2D;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Tarea que realiza una de las operaciones conocidas de forma aleatoria
 * y el agente recoge el resultado.
 * @author pedroj
 */
public class TareaRealizarOperacion extends OneShotBehaviour {
    private RealizarOperacion agente;

    public TareaRealizarOperacion(Agent a) {
        super(a);
        this.agente = (RealizarOperacion) a;
    }
    
    @Override
    public void action() {
        Punto2D punto = agente.getPunto2D();
        agente.addResultado(operacion(punto));
    }
    
    private String operacion (Punto2D punto) {
        double resultado;
        
        //Se realiza una operación elegida de forma aleatoria

        Operacion opSeleccionada = Constantes.Operacion.getOperacion(); 
        switch ( opSeleccionada.ordinal() ) {
            case 0:
                //Suma
                resultado = punto.getX() + punto.getY();
                return "Se ha realizado la suma de " + punto
                        + "\ncon el resultado: "+ resultado;
            case 1:
                //Resta
                resultado = punto.getX() - punto.getY();
                return "Se ha realizado la resta de " + punto
                        + "\ncon el resultado: "+ resultado;
            case 2:
                //Multiplicación
                resultado = punto.getX() * punto.getY();
                return "Se ha realizado la multiplicación de " + punto
                        + "\ncon el resultado: "+ resultado;
            default:
                //División
                if( punto.getY() != 0 ) {
                    resultado = punto.getX() / punto.getY();
                    return "Se ha realizado la división de " + punto
                        + "\ncon el resultado: "+ resultado;
                } else
                    return "OPERACION NO DEFINIDA";
        }
    }
}
