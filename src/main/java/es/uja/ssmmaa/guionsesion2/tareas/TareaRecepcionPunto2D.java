/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import static es.uja.ssmmaa.guionsesion2.Constantes.PRIMERO;
import static es.uja.ssmmaa.guionsesion2.Constantes.SEGUNDO;
import es.uja.ssmmaa.guionsesion2.util.Punto2D;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Tarea que recoge un mensaje y lo transfora a un Punto2D, si es posible, y
 * lo pasa al agente para que posteriormente sea procesado por TareaRealizarOperacion
 * @author pedroj
 */
public class TareaRecepcionPunto2D extends CyclicBehaviour {
    private RecepcionPunto2D agente;
    private AID agenteDF;

    public TareaRecepcionPunto2D(Agent a, AID agenteDF) {
        super(a);
        this.agente = (RecepcionPunto2D) a;
        this.agenteDF = agenteDF;
    }

    @Override
    public void action() {
        Punto2D punto = new Punto2D();
        
        //Recepción de la información para realizar la operación
        MessageTemplate plantilla = MessageTemplate.and(
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
                    MessageTemplate.not(MessageTemplate.MatchSender(agenteDF)));
        ACLMessage mensaje = myAgent.receive(plantilla);
        if (mensaje != null) {
            //procesamos el mensaje
            String[] contenido = mensaje.getContent().split(",");
                
            try {
                punto.setX(Double.parseDouble(contenido[PRIMERO]));
                punto.setY(Double.parseDouble(contenido[SEGUNDO]));
                
                agente.addPunto2D(punto);
                
                myAgent.addBehaviour(new TareaRealizarOperacion(myAgent));
            } catch (NumberFormatException ex) {
                // No sabemos tratar el mensaje y los presentamos por consola
                System.out.println("El agente: " + myAgent.getName() + 
                            " no entiende el contenido del mensaje: \n\t"
                            + mensaje.getContent() + " enviado por: \n\t"
                            + mensaje.getSender());
            }
        } else
            block();
    }
}
