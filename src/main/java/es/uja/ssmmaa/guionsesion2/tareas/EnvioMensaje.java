/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio;
import jade.core.AID;

/**
 * Métodos que debe implementar el agente que quiera enviar mensajes a un
 * agente por medio de TareaEnvioMensaje.
 * @author pedroj
 */
public interface EnvioMensaje {
    public AID getAgent(NombreServicio servicio);
    public String getMensaje();
}
