/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import static es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio.OPERACION;
import es.uja.ssmmaa.guionsesion2.util.Punto2D;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.List;

/**
 * Tarea para el envio de un Punto2D a todos los agentes de servicio
 * OPERACION conocidos por el agente.
 * @author pedroj
 */
public class TareaEnvioOperacion extends OneShotBehaviour {
    private EnvioOperacion agente;
    private Punto2D punto;

    public TareaEnvioOperacion(Agent a, Punto2D punto) {
        super(a);
        this.agente = (EnvioOperacion) a;
        this.punto = punto;
    }

    @Override
    public void action() {
        // Se crea el mensaje
        ACLMessage mensaje = new ACLMessage(ACLMessage.INFORM);
        mensaje.setSender(myAgent.getAID());
        
        // Se añaden todos los agentes operación
        List<AID> listaAgentes = agente.getAgentesOperacion(OPERACION);
        for( AID receptor : listaAgentes )
            mensaje.addReceiver(receptor);
        
        // Se crea el contenido
        mensaje.setContent(punto.getX() + "," + punto.getY());
        
        // Se realiza el envío
        myAgent.send(mensaje);
            
        //Se añade el contenido del mensaje para la consola
        agente.addMensaje("Enviado a: " + listaAgentes.size() +
                    " agentes el punto: " + mensaje.getContent());
    }
}
