/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.gui.ConsolaJFrame;
import es.uja.ssmmaa.guionsesion2.util.MensajeConsola;

/**
 * Comportamiento que debe implementar un agente que quiera presentar un
 * MensajeConsola en una ventana ConsolaJFrame por medio de TareaPresentarMensaje.
 * @author pedroj
 */
public interface PresentarMensaje {
    public MensajeConsola getMensaje();
    public ConsolaJFrame getGui(String nameAgent);
    public void addGui(ConsolaJFrame gui);
    
}
