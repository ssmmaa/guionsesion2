/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio;
import jade.core.AID;
import java.util.List;

/**
 * Métodos que debe implementar un agente que quiera utiliza TareaEnvioOperacion
 * @author pedroj
 */
public interface EnvioOperacion {
    public List<AID> getAgentesOperacion(NombreServicio servicio);
    public void addMensaje(String contenido);
}
