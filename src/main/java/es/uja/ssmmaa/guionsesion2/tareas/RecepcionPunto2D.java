/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.util.Punto2D;

/**
 * Método que debe implementar el agente para trabajar con TareaRecepcionPunto2D
 * @author pedroj
 */
public interface RecepcionPunto2D {
    public void addPunto2D(Punto2D punto);
}
