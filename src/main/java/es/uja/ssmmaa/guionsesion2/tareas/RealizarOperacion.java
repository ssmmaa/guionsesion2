/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.tareas;

import es.uja.ssmmaa.guionsesion2.util.Punto2D;

/**
 * Métodos que debe implementar un agente que utilice TareaRealizarOperacion
 * @author pedroj
 */
public interface RealizarOperacion {
    public void addResultado(String contenido);
    public Punto2D getPunto2D();
}
