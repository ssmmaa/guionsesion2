/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.agentes;

import static es.uja.ssmmaa.guionsesion2.Constantes.REPETICION;
import static es.uja.ssmmaa.guionsesion2.Constantes.SERVICIOS;
import es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio;
import static es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio.OPERACION;
import static es.uja.ssmmaa.guionsesion2.Constantes.PRIMERO;
import static es.uja.ssmmaa.guionsesion2.Constantes.TIPO_SERVICIO;
import es.uja.ssmmaa.guionsesion2.gui.FormularioJFrame;
import es.uja.ssmmaa.guionsesion2.tareas.EnvioMensaje;
import es.uja.ssmmaa.guionsesion2.tareas.EnvioOperacion;
import es.uja.ssmmaa.guionsesion2.tareas.SuscripcionDF;
import es.uja.ssmmaa.guionsesion2.tareas.TareaEnvioMensaje;
import es.uja.ssmmaa.guionsesion2.tareas.TareaEnvioOperacion;
import es.uja.ssmmaa.guionsesion2.tareas.TareaSuscripcionDF;
import es.uja.ssmmaa.guionsesion2.util.Punto2D;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteFormulario extends Agent implements EnvioMensaje, EnvioOperacion,
                                                        SuscripcionDF {
    //Variables del agente
    private FormularioJFrame myGui;
    private ArrayList<AID>[] listaAgentes;
    private ArrayList<String> mensajesPendientes;

    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       mensajesPendientes = new ArrayList();
       listaAgentes = new ArrayList[SERVICIOS.length];
       for( NombreServicio servicio : SERVICIOS )
           listaAgentes[servicio.ordinal()] = new ArrayList();
       
       //Configuración del GUI
       myGui = new FormularioJFrame(this);
       myGui.setVisible(true);
       
       //Registro del agente en las Páginas Amarrillas
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
       addBehaviour(new TareaEnvioMensaje(this,REPETICION));
       
       //Suscripción al servicio de páginas amarillas
       //Para localiar a los agentes operación y consola
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(TIPO_SERVICIO);
       template.addServices(templateSd);
       addBehaviour(new TareaSuscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       
       //Liberación de recursos, incluido el GUI
       myGui.dispose();
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    public void enviarPunto2D(Punto2D punto) {
        addBehaviour(new TareaEnvioOperacion(this,punto));
    }
    
    @Override
    public AID getAgent(NombreServicio servicio) {
        if( listaAgentes[servicio.ordinal()].isEmpty() )
            return null;
        else 
            return listaAgentes[servicio.ordinal()].get(PRIMERO);
    }

    @Override
    public String getMensaje() {
        if( mensajesPendientes.isEmpty() )
            return null;
        else
            return mensajesPendientes.remove(PRIMERO);
    }

    @Override
    public List<AID> getAgentesOperacion(NombreServicio servicio) {
        return listaAgentes[servicio.ordinal()];
    }

    @Override
    public void addMensaje(String contenido) {
        mensajesPendientes.add(contenido);
    }

    @Override
    public void addAgent(AID agente, NombreServicio servicio) {
        listaAgentes[servicio.ordinal()].add(agente);
        
        if ( !listaAgentes[OPERACION.ordinal()].isEmpty() )
            myGui.activarEnviar(true);
    }

    @Override
    public boolean removeAgent(AID agente, NombreServicio servicio) {
        boolean resultado = listaAgentes[servicio.ordinal()].remove(agente);
        
        if ( listaAgentes[OPERACION.ordinal()].isEmpty() )
            myGui.activarEnviar(false);
        
        return resultado;
    }
}
