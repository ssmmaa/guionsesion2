/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2.agentes;

import es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio;
import static es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio.CONSOLA;
import static es.uja.ssmmaa.guionsesion2.Constantes.NombreServicio.OPERACION;
import static es.uja.ssmmaa.guionsesion2.Constantes.PRIMERO;
import static es.uja.ssmmaa.guionsesion2.Constantes.REPETICION;
import static es.uja.ssmmaa.guionsesion2.Constantes.TIPO_SERVICIO;
import es.uja.ssmmaa.guionsesion2.tareas.EnvioMensaje;
import es.uja.ssmmaa.guionsesion2.tareas.RealizarOperacion;
import es.uja.ssmmaa.guionsesion2.tareas.RecepcionPunto2D;
import es.uja.ssmmaa.guionsesion2.tareas.SuscripcionDF;
import es.uja.ssmmaa.guionsesion2.tareas.TareaEnvioMensaje;
import es.uja.ssmmaa.guionsesion2.tareas.TareaRecepcionPunto2D;
import es.uja.ssmmaa.guionsesion2.tareas.TareaSuscripcionDF;
import es.uja.ssmmaa.guionsesion2.util.Punto2D;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteOperacion extends Agent implements RealizarOperacion, RecepcionPunto2D,
                                            SuscripcionDF, EnvioMensaje {
    //Variables del agente
    private ArrayList<AID> agentesConsola;
    private ArrayList<String> mensajesPendientes;
    private ArrayList<Punto2D> operacionesPendientes;
    private AID agenteDF;

    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       mensajesPendientes = new ArrayList();
       operacionesPendientes = new ArrayList();
       agentesConsola = new ArrayList();
       agenteDF = new AID("df", AID.ISLOCALNAME);
       
       //Configuración del GUI
       
       //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
	ServiceDescription sd = new ServiceDescription();
	sd.setType(TIPO_SERVICIO);
	sd.setName(OPERACION.name());
	dfd.addServices(sd);
	try {
            DFService.register(this, dfd);
	}
	catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       
       //Añadir las tareas principales
       addBehaviour(new TareaRecepcionPunto2D(this, agenteDF));
       addBehaviour(new TareaEnvioMensaje(this,REPETICION));
       
       //Suscripción al servicio de páginas amarillas
       //Para localiar a los agentes operación y consola
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(TIPO_SERVICIO);
       templateSd.setName(CONSOLA.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSuscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
        try {
            DFService.deregister(this);
	}
            catch (FIPAException fe) {
            fe.printStackTrace();
	}
       
       //Liberación de recursos, incluido el GUI
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    @Override
    public void addResultado(String contenido) {
        mensajesPendientes.add(contenido);
    }

    @Override
    public Punto2D getPunto2D() {
        return operacionesPendientes.remove(PRIMERO);
    }

    @Override
    public void addPunto2D(Punto2D punto) {
        operacionesPendientes.add(punto);
    }

    @Override
    public void addAgent(AID agente, NombreServicio servicio) {
        agentesConsola.add(agente);
    }

    @Override
    public boolean removeAgent(AID agente, NombreServicio servicio) {
        return agentesConsola.remove(agente);
    }

    @Override
    public AID getAgent(NombreServicio servicio) {
        if( agentesConsola.isEmpty() )
            return null;
        else
            return agentesConsola.get(PRIMERO);
    }

    @Override
    public String getMensaje() {
        if( mensajesPendientes.isEmpty() )
            return null;
        else
            return mensajesPendientes.remove(PRIMERO);
    }
}
