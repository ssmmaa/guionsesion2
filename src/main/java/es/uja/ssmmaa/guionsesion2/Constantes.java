/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.guionsesion2;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final long REPETICION = 10000; // 10 segundos
    public static final int NO_ENCONTRADO = 0;
    public static final int PRIMERO = 0;
    public static final int SEGUNDO = 1;
    public static final int D100 = 100; // Representa un dado de 100
    
    public static final String TIPO_SERVICIO = "Agentes Servicio";
    public enum NombreServicio {
        OPERACION, CONSOLA;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Operacion {
        SUMA(25), RESTA(50), MULTIPLICACION(75), DIVISION(100);
        
        private int valor;

        private Operacion(int valor) {
            this.valor = valor;
        }
        
        /**
         * Devuelve una operación de las disponibles aleatoriamente
         * @return Operacion; una de las operaciones disponibles
         */
        public static Operacion getOperacion() {
            Random aleatorio = new Random();
            int tiradaDado = aleatorio.nextInt(D100);
            
            for( Operacion operacion : Operacion.values() ) {
                if( operacion.valor > tiradaDado )
                    return operacion;
            }
            
            return DIVISION;
        }
    }
    public static final Operacion[] OPERACIONES = Operacion.values();
}
