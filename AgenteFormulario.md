# `AgenteFormulario`

Este agente tendrá asociado un formulario para que el usuario pueda dar dos valores que formarán un `Punto2D` que se enviará a los `AgenteOperacion` conocidos, si se no hay `AgenteOperación` no podrá enviar el `Punto2D`, y enviará un mensaje a los `AgenteConsola` de las solicitudes de operación enviadas. Si no se localizan `AgentesConsola` se almacenan los mensajes. Para poder completar sus tareas debe suscribirse al servicio de páginas amarillas para que le informe de los `AgenteConsola` y `AgenteOperación` presentes en la plataforma.

Para el diseño del agente serán necesarias las siguientes variables de instancia:

- Una que nos permita almacenar un objeto `FormularioJFrame` que permite interactuar al usuario.
- Una lista de los agentes conocidos.
- Una lista para los mensajes que hay que enviar a un `AgenteConsola`.

El diseño de los métodos y tareas del agente será:

### Método `setup()`

Se inicializan las variables de instancia. Para la lista de los agentes conocidos la dividiremos según el tipo disponibles, es decir, `AgenteConsola` y `AgenteOperación`:

```java
...
listaAgentes = new ArrayList[SERVICIOS.length];
for( NombreServicio servicio : SERVICIOS )
    listaAgentes[servicio.ordinal()] = new ArrayList();
...
```

A continuación, inicializamos la interface del agente mediante el objeto `FormularioJFrame`.  Añadimos la tarea que enviará los mensajes a un `AgenteConsola` y la suscripción al servicio de páginas amarillas con los tipos de agente que queremos localizar.

```java
...
//Suscripción al servicio de páginas amarillas
//Para localiar a los agentes operación y consola
DFAgentDescription template = new DFAgentDescription();
ServiceDescription templateSd = new ServiceDescription();
templateSd.setType(TIPO_SERVICIO);
template.addServices(templateSd);
addBehaviour(new TareaSuscripcionDF(this,template));
...
```

### Método `takeDown()`

Solo hay que liberar la interface `FormularioJFrame` asociada.

### Métodos auxiliares

Vamos a necesitar un método público para que la interface `FormularioJFrame` pueda añadir una tarea que envíe el Punto2D a todos los `AgentesOperacion` conocidos. 

```java
...
    //Métodos de trabajo del agente
    public void enviarPunto2D(Punto2D punto) {
        addBehaviour(new TareaEnvioOperacion(punto));
    }
...
```

## Clase `TareaEnvioOperacion`

Es una tarea que se realiza una vez por cada Punto2D que será enviado a los `AgenteOperacion` y que es añadido por el método que se ha expuesto anteriormente.

```java
    public class TareaEnvioOperacion extends OneShotBehaviour {
        private Punto2D punto;

        public TareaEnvioOperacion(Punto2D punto) {
            this.punto = punto;
        }
        

        @Override
        public void action() {
            //Se envía la operación a todos los agentes operación
            ACLMessage mensaje = new ACLMessage(ACLMessage.INFORM);
            mensaje.setSender(myAgent.getAID());
            //Se añaden todos los agentes operación
            int numAgentes = listaAgentes[OPERACION.ordinal()].size();
            for (int i=0; i < numAgentes; i++) {
                mensaje.addReceiver(listaAgentes[OPERACION.ordinal()].get(i));
            }
            mensaje.setContent(punto.getX() + "," + punto.getY());
            
            send(mensaje);
            
            //Se añade el mensaje para la consola
            mensajesPendientes.add("Enviado a: " + numAgentes +
                    " agentes el punto: " + mensaje.getContent());
        }
    }
```

## Clase `TareaEnvioConsola`

Es una tarea que se repite cíclicamente pasado un tiempo representado por `REPETICION`. Cada vez que se activa envía el primer mensaje de la lista al primer `AgenteConsola` conocido. Si no hay mensajes no hace nada.

```java
    public class TareaEnvioConsola extends TickerBehaviour {

        public TareaEnvioConsola(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            ACLMessage mensaje;
            if (!listaAgentes[CONSOLA.ordinal()].isEmpty()) {
                if (!mensajesPendientes.isEmpty()) {
                    mensaje = new ACLMessage(ACLMessage.INFORM);
                    mensaje.setSender(myAgent.getAID());
                    mensaje.addReceiver(listaAgentes[CONSOLA.ordinal()].get(PRIMERO));
                    mensaje.setContent(mensajesPendientes.remove(PRIMERO));
            
                    myAgent.send(mensaje);
                }
                else {
                    //Si queremos hacer algo si no tenemos mensajes
                    //pendientes para enviar a la consola
                }
            }
        }
    }
```

## Clase `TareaSuscripcionDF`

El agente se suscribe al servicio de páginas amarillas para que sea notificado cada vez que un `AgenteConsola` o `AgenteFormulario` se registre o abandone el registro del servicio. Por motivos de depuración se envía un mensaje a la consola con los agentes conocidos. Se activará o desactivará el botón de la interfaz `FormularioJFrame` según se localicen o no `AgenteOperación`.

```java
    public class TareaSuscripcionDF extends DFSubscriber {

        public TareaSuscripcionDF(Agent a, DFAgentDescription template) {
            super(a, template);
        }

        @Override
        public void onRegister(DFAgentDescription dfad) {
            Iterator it = dfad.getAllServices();
            while( it.hasNext() ) {
                ServiceDescription sd = (ServiceDescription) it.next();
                for( NombreServicio servicio : SERVICIOS )
                    if( sd.getName().equals(servicio.name()) )
                        listaAgentes[servicio.ordinal()].add(dfad.getName());
            }
            
            // Si hemos localizado agentes de operación activamos el botón de
            // envío
            if( !listaAgentes[OPERACION.ordinal()].isEmpty() )
                myGui.activarEnviar(true);
            
            System.out.println("El agente: " + myAgent.getName() +
                    "ha encontrado a:\n\t" + Arrays.toString(listaAgentes));
        }

        @Override
        public void onDeregister(DFAgentDescription dfad) {
            AID agente = dfad.getName();
            
            for( NombreServicio servicio : SERVICIOS )
                if( listaAgentes[servicio.ordinal()].remove(agente) )
                    System.out.println("El agente: " + agente.getName() + 
                            " ha sido eliminado de la lista de " 
                            + myAgent.getName());
            
            // Si no hay agentes de operación inactivamos el botón de envío
            if( !listaAgentes[OPERACION.ordinal()].isEmpty() )
                myGui.activarEnviar(false);
        }        
    }
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0NjM1MzIzOTJdfQ==
-->