# `AgenteConsola`

El objetivo principal de este agente es presentar los mensajes que reciba de los distintos agentes y mostrarlos en una `ConsolaJFrame` para cada uno de ellos. Las variables de instancia del agente serán:

- Una lista para contener todos los objetos `ConsolaJFrame` que se creen.
- Una lista con los mensajes que no se han añadido aún a la `ConsolaJFrame` correspondiente.

Ahora pasamos con el diseño del agente y el diseño de sus tareas:

### `Método setup()`

Inicializamos las variables de instancia, registramos la actividad de la consola en el servicio de páginas amarillas y por último añadimos la tarea del agente. Se muestra el código necesario para el registro del agente en el servicio de páginas amarillas.

```java
...
//Registro del agente en las Páginas Amarrillas
DFAgentDescription dfd = new DFAgentDescription();
dfd.setName(getAID());
ServiceDescription sd = new ServiceDescription();
sd.setType(TIPO_SERVICIO);
sd.setName(CONSOLA.name());
dfd.addServices(sd);
try {
    DFService.register(this, dfd);
} catch (FIPAException fe) {
    fe.printStackTrace();
}
...
```

### Método `takeDown()`

Este método eliminará el registro en el servicio de páginas amarillas y liberará todos los recursos asociados del agente. 

### Métodos auxiliares

Para mejorar la legibilidad del agente se han diseñado dos métodos auxiliares, uno para eliminar todos los objetos `ConsolaJFrame` y otro para localizar el objeto `ConsolaJFrame` asociado a un agente, para no crear más de uno por agente.

```java
...
    //Métodos de utilidad para el agente consola
    private ConsolaJFrame buscarConsola(String nombreAgente) {
        // Obtenemos la consola donde se presentarán los mensajes
        for( ConsolaJFrame gui : myGui) 
            if (gui.getNombreAgente().compareTo(nombreAgente) == NO_ENCONTRADO)
                return gui;
                    
        return null;
    }
    
    private void cerrarConsolas() {
        //Se eliminan las consolas que están abiertas
        for( ConsolaJFrame gui : myGui )
            gui.dispose();
    }
...
```

## Clase `TareaRecepcionMensajes`

Es una clase interna al agente para recibir los mensajes que envían los agentes y que posteriormente serán presentados en su `ConsolaJFrame` asociada. Esta tarea se añadirá en el método `setup()` y es una tarea cíclica, estará activa mientras el agente esté presente en la plataforma.

```java
    public class TareaRecepcionMensajes extends CyclicBehaviour {

        @Override
        public void action() {
            //Solo se atenderán mensajes INFORM
            MessageTemplate plantilla = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage mensaje = myAgent.receive(plantilla);
            if (mensaje != null) {
                //procesamos el mensaje
                MensajeConsola mensajeConsola = new MensajeConsola(mensaje.getSender().getName(),
                                    mensaje.getContent());
                mensajesPendientes.add(mensajeConsola);
                addBehaviour(new TareaPresentarMensaje());
            } 
            else
                block();
        }
    }
```

Una cosa importante es que este tipo de tareas solo debería recoger el mensaje y almacenarlo en una variable del agente para que posteriormente sea tratado por otra tarea. En este caso se añadirá una tarea específica para cada mensaje que se recibe.

## Clase `TareaPresentarMensaje`

Esta será una tarea que se realizará una única vez para tratar un único mensaje que se presentará en el objeto `ConsolaJFrame` apropiado. 

```java
...
    public class TareaPresentarMensaje extends OneShotBehaviour {

        @Override
        public void action() {
            //Se coge el primer mensaje
            MensajeConsola mensajeConsola = mensajesPendientes.remove(PRIMERO);
            
            //Se busca la ventana de consola o se crea una nueva
            ConsolaJFrame gui = buscarConsola(mensajeConsola.getNombreAgente());
            if (gui == null) {
                gui = new ConsolaJFrame(mensajeConsola.getNombreAgente());
                myGui.add(gui);
            } 
            
            gui.presentarSalida(mensajeConsola);
        }
    }
...
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbODYzNjkwNTFdfQ==
-->