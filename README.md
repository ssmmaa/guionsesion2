[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Guión de Prácticas
## 2. Primera comunidad de agentes

En la [Sesión 1](https://gitlab.com/ssmmaa/guionsesion1/-/blob/master/README.md) se ha mostrado la estructura que debe tener un proyecto para el desarrollo de agentes utilizando la biblioteca [Jade](https://jade.tilab.com/). Como punto de partida comenzamos desde el [proyecto](https://gitlab.com/ssmmaa/guionsesion2/-/archive/v0.1/guionsesion2-v0.1.zip) con la estructura de un agente que se modificará hasta conseguir un conjunto de agentes que se comunicarán para resolver un problema simple. También tendremos que completar el proyecto con la creación de la interfaces asociadas a cada uno de los agentes y con algunas clases de utilidad.

Antes de empezar vamos a repasar algunos elementos necesarios en el diseño de los agentes que realizaremos a lo largo del resto de las prácticas de la asignatura.

## 2.1 Ciclo de vida de un agente

![][cicloVida]

Es muy importante que tengamos presente el ciclo de vida de los agentes que diseñemos y la forma en que se comportan una vez que se han creado en la plataforma de agentes. Como se muestra en la imagen tenemos que tener presente lo siguiente:

- Inicialización del agente.
- Ejecución del comportamiento del agente.
- Finalización del agente.

### Inicialización del agente

Cuando el agente es creado en la plataforma se invoca el método `setup()`. En el método incluiremos el código necesario para la correcta inicialización del agente. Como vimos en el [guión de la sesión anterior](https://gitlab.com/ssmmaa/guionsesion1/-/blob/master/README.md#m%C3%A9todo-setup) deberemos realizar las siguientes acciones, dependiendo de las necesidades propias del agente, para una correcta inicialización del agente:

- Inicialización de las variables del agente.
- Configuración de la interface que disponga el agente. Debería incluir un mensaje de presentación del agente.
- Registro del agente en el servicio de páginas amarillas.
- Registro de la ontología para las necesidades de comunicación del agente.
- Añadir las tareas principales que representan el comportamiento inicial que tendrá nuestro agente.

Una vez que se completan todas la líneas de código presentes en el método `setup()` la inicialización del agent está completa y pasará a ejecutar el comportamiento que se ha programado. 

### Ejecución del comportamiento del agente

En este estado estará el agente permanentemente hasta que no sea eliminado de la plataforma. De este estado el agente saldrá por los siguientes motivos:

- La finalización de la plataforma de agentes.
- Finalización del agente porque se invoque el método `doDelete()`.
- Una excepción del agente. Si se produce esta circunstancia siempre de debe dar una solución programada para que el agente finalice correctamente, es decir, como si se hubiera solicitado su finalización.

Otra consideración que hay que hacer es que el agente tiene asignado un **único hilo de ejecución** para que complete sus tareas, objetos que heredan de la clase [Behaviours](https://jade.tilab.com/doc/api/jade/core/behaviours/Behaviour.html), y de esta forma solo una única tarea se ejecutará a la vez. Esto nos ayudará en el diseño de las tareas al evitar los problemas de **exclusión mutua** en el acceso de las variables del agente. Pero sí tendremos que tener presente los problemas de **sincronización** entre las tareas, no podemos saber el orden en que se ejecutarán las tareas. Cualquiera de las que esté disponible podrá ser elegida para su ejecución.

El ciclo de ejecución para una tarea se comporta de la siguiente forma:

1. Se elige una de las tareas disponibles, tareas que estén activas, para su ejecución.
2. De la tarea seleccionada se invoca su método `action()`. Este método tiene el código que representa lo que debe hacer una tarea cuando se ejecute.
3. A la finalización del método `action()` se invoca el método `done()` de la tarea en ejecución. El método `done()` devolverá un valor `true` si la tarea se ha completado, es decir, ya ha finalizado con todo lo que debe hacer. Cuando una tarea finaliza es eliminada del conjunto de tareas que el agente puede ejecutar. Si el método `done()` devuelve `false` quiere decir que la tarea no ha finalizado todo lo que debe hacer y por tanto seguirá disponible entre las tareas que puede ejecutar el agente.

Estos tres pasos se estarán repitiendo hasta que el agente finalice.

### Finalización del agente

Cuando un agente finaliza se invoca su método `takeDown()`. En el método tendrá todo el código necesario para una *finalización ordenada del agente* al abandonar la plataforma. Como ya presentamos en el [guión de la sesión anterior](https://gitlab.com/ssmmaa/guionsesion1/-/blob/master/README.md#m%C3%A9todo-takedown) las acciones que debemos tener presente en este método serán las siguientes:

- Eliminar el registro del agente en el servicio de páginas amarillas.
- Liberar todos los recursos asociados al agente.
- Mensaje de despedida del agente. Este mensaje se realizará en la `salida estándar` por motivos de depuración y así comprobar de forma efectiva que el agente ha finalizado como tenemos programado.

Ya que he recordado el ciclo de vida de un agente voy a recordar también el diseño general que tenemos que tener en cuenta para la programación de las tareas que debe realizar un agente. Esas tareas serán el comportamiento que presenta el agente y que permite completar los objetivos para los que ha sido diseñado.

## 2.2 Diseño de las tareas de un agente

Como ya he comentado anteriormente, todas las tareas de un agente serán especializaciones de la clase [Behaviour](https://jade.tilab.com/doc/api/jade/core/behaviours/Behaviour.html). Es conveniente repasar la documentación de programación de esta clase para comprender todas las posibilidades que tenemos a nuestra disposición en el diseño de las tareas de un agente. Para esta práctica, y para la mayoría, solo tendremos que tener presente los siguientes métodos:

### Método `action()`

Este será el método que se invoca en el ciclo de vida del agente cada vez que la tarea se ejecuta. El método estará diseñado para todo lo que deba realizar la tarea cuando sea seleccionada para su ejecución. Solo incluiremos el código necesario para eso, es decir, que la tarea tendrá un comportamiento relacionado con el estado de las variables de la tarea y/o agente que tiene asociado.

### Método `done()`

Cada vez que se completa la ejecución del método `action()` de la tarea será invocado. El método deberá devolver el valor `true` si la tarea ya se ha completado. Es decir, ya no necesitamos una nueva ejecución de la tarea por parte del agente. La tarea ya no se volverá a ejecutar. El método devolverá el valor `false` si no se ha completado y podrá volver a ser ejecutada por el agente más adelante. Es decir, el método `action()` se volverá a ejecutar en otro momento en el ciclo de vida del agente.

Para el diseño de las tareas de un agente tenemos dos opciones:

1. Diseñar la tarea como una clase individual y deberemos tener una referencia asociada para representar al agente que está ejecutando la tarea. Será necesario definir métodos de acceso para las variables del agente. Si el tipo de tarea es compartido por más de un agente es la solución más adecuada.
2. Diseñar la tarea como una clase interna de la clase del agente. De esta forma la tarea tiene acceso a las variables del agente sin necesidad de métodos de acceso. Si la tarea es exclusiva del agente es la solución más adecuada.

### Clases especializadas de `Behaviour`

Tenemos a nuestra disposición unas clases especializadas de `Behaviour`, para algunas tareas comunes, que nos pueden resultar muy útiles en nuestro diseño:

- [OneShotBehaviour](https://jade.tilab.com/doc/api/jade/core/behaviours/OneShotBehaviour.html): Tarea que se realiza una vez:
	- Solo tenemos que implementar el método `action()`. El método `done()` devuelve siempre `true`.
- [WakerBehaviour](https://jade.tilab.com/doc/api/jade/core/behaviours/WakerBehaviour.html): Tarea que se realiza una vez pero estará disponible solo cuando haya pasado un periodo de tiempo definido en el constructor.
	- El método que debemos implementar es `handleElapsedTimeout()` que se invocará pasado el periodo de tiempo definido. Por lo demás se comporta igual que `OneShotBehaviour`.
- [CyclicBehaviour](https://jade.tilab.com/doc/api/jade/core/behaviours/CyclicBehaviour.html): Tarea que se realiza indefinidamente, es decir, nunca finaliza.
	- Solo tenemos que implementar el método `action()`. El método `done()` devuelve siempre `false`.
- [TickerBehaviour](https://jade.tilab.com/doc/api/jade/core/behaviours/TickerBehaviour.html): Tarea que se realiza indefinidamente pero solo estará disponible en el periodo de tiempo que se definió en el constructor. 
	- El método que debemos implementar es el `onTick()`. Estará disponible indefinidamente pero solo pasado el periodo de tiempo definido en el constructor. Por lo demás funciona igual que la tarea cíclica, pero muchas veces puede ser más apropiado adoptar este tipo de tareas si queremos que no finalice.

### Añadir una tarea para que un agente la ejecute

Para que un agente pueda ejecutar una tarea se tiene que ejecutar el método `addBehaviour(.)` sobre la instancia del agente. La invocación de este método puede realizarse en cualquier momento en el diseño del agente y también puede invocarse en una tarea que esté ejecutando un agente. Este método añade la tarea al conjunto de tareas que debe ejecutar el agente, es decir, estará disponible para su ejecución en el ciclo de vida del agente. No que que se ejecute inmediatamente después de la invocación del método `addBehaviour(.)`. En el ejemplo que se presenta en este guión podrá observarse diferentes momentos en el que una tarea será añadida al conjunto de tareas de un agente.

## 2.3 Comunicación entre agentes

Como el objetivo de esta sesión es el diseño de agentes que deban comunicarse hay que presentar la forma en que debemos hacerlo en la implementación de Jade. La implementación de la biblioteca de agentes [Jade](https://jade.tilab.com/) se base en el estándar [FIPA](http://www.fipa.org/) y la arquitectura que tendrá es la que se presenta a continuación:

![][arquitecturaJade]

Esta arquitectura de define la plataforma donde se ejecutarán los agentes. La plataforma es distribuida y dispondrá de diferentes contenedores donde se albergan los agentes. Siempre cuenta con un contenedor principal, y solo uno por plataforma, donde se albergan unos agentes especializados:

- **AMS**: Es el agente encargado de la gestión de la plataforma y es único por plataforma.
- **DF**: Es el agente encargado del servicio de páginas amarillas de la plataforma donde los agentes presentes pueden registrarse para poder ser localizados por cualquier agente. Ya sea de la plataforma o de otra plataforma.

Como la arquitectura se basa en un modelo de **sistema distribuido**, los agentes deberán comunicarse para poder compartir información. Esta comunicación será llevada a cabo por un agente especializado en la plataforma que recogerá el mensaje del agente emisor y lo depositará en el buzón del agente receptor, si es de la misma plataforma, o se lo pasará al agente de comunicación de la plataforma, del agente receptor, y lo depositará en le buzón. 

![][comunicacionAgentes]

Una vez el agente emisor envía el mensaje puede seguir con su ejecución normalmente. El agente receptor procesará los mensajes del buzón según el diseño que tenga de sus tareas, es decir, la **comunicación es asíncrona**. Pero el agente emisor sí recibirá una excepción si el mensaje no ha podido ser entregado al receptor.

### El lenguaje ACL para los mensajes

El estándar [ACL](http://www.fipa.org/repository/aclspecs.html), *Agent Communication Language*, tiene definida una estructura para los mensajes que podéis repasar en la documentación. Solo voy a presentar las partes que serán necesarias para el desarrollo de la práctica.

Este tipo de comunicación no se basa en el intercambio de datos, el contenido del mensaje representa un contenido de información, y eso tenemos que tenerlo muy en cuenta en el diseño de nuestros agentes y las necesidades de información para que puedan completar sus tareas. Una ventaja que tiene ACL es que es directamente *imprimible*, es decir, podemos leer nosotros mismos el mensaje y entenderlo. No necesitamos que nuestros agentes lo procesen previamente para dar ese *formato imprimible*. Eso nos ayudará a depurar el diseño de nuestros agentes.

Las partes que nos importan ahora mismo de un mensaje ACL, que es un objeto representado por la clase [ACLMessage](https://jade.tilab.com/doc/api/jade/core/AID.html), son las siguientes:

- El **emisor**: representa al agente que envía el mensaje y es un objeto de la clase [AID](https://jade.tilab.com/doc/api/jade/core/AID.html). Es la identificación del agente dentro de la arquitectura de Jade.
- Lista de **receptores**: representa a todos los posibles receptores del mensaje. También son objetos `AID`, como podemos ver el mismo mensaje puede ser enviado a más de un agente. 
- **Intención** del mensaje: nos permitirá identificar el significado que tiene el mensaje. En las clases teóricas de la asignatura dedicaré más tiempo para explicar con detenimiento este elemento. Lo importante que debemos tener presente es que el contenido del mensaje representa información y no exclusivamente datos. Este elemento es una constante que tendremos a nuestra disposición en la clase `ACLMessage`.
- El **contenido** del mensaje: es la información que queremos transmitir. Estará representado por un objeto de la clase `String` de Java. Más adelante en las prácticas, y en teoría, veremos la forma de construir este contenido basándonos en una **Ontología** que nos permitirá representar la información a transmitir con una estructura que pueda ser interpretada sin equivocaciones por los agentes.

### Envío de un mensaje

La emisión de los mensajes de un agente deben estar definidos dentro de una tarea especializada o definir una única tarea de emisión que se adapte al tipo de mensaje que se deba enviar.  El código necesario para enviar un mensaje será el siguiente:

```java
...
ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
msg.addReceiver(new AID(“Peter”, AID.ISLOCALNAME)); 
msg.setContent(“Today it’s raining”);
send(msg);
...
```

Desde este momento el mensaje pasa a la plataforma para que sea entregado en el buzón de los receptores.

### Recepción de un mensaje

Al igual que en el caso anterior, debemos diseñar una tarea especializada para la recepción del mensaje o una tarea general que lea del buzón y añada la tarea específica para el tratamiento del mensaje. Lo que tenemos que tener presente es que el buzón donde se reciben los mensajes es único por agente y que recoger un mensaje lo elimina del buzón. Además, al tratarse de **comunicación asíncrona**, no podemos garantizar que haya mensaje cuando exploramos el buzón. El código necesario para recoger un mensaje del buzón será el siguiente:

```java
...
ACLMessage msg = myAgent.receive(); 
if (msg != null) { 
    // Se ha recibido el mensaje y lo procesamos 
} else {
    // No hay mensaje y la tarea se bloquea
    block(); 
}
...
```

Si no hay mensaje disponible debemos bloquear la tarea para que no consuma recursos de ejecución. Cada vez que se reciba un mensaje en el buzón se envía `notifyAll()` y se activan todas las tareas bloqueadas. Es importante tener esto presente para que comprobemos si se ha recibido algún mensaje. Dado que puede haber más de una tarea que reciba mensajes y no haber mensajes para todas cuando se activen.

Si hemos diseñado una tarea específica para un tipo de mensaje no podemos adoptar el código que se ha presentado anteriormente, ese código retira cualquier mensaje, para ello tenemos a nuestra disposición la clase [MessageTemplate](https://jade.tilab.com/doc/api/jade/lang/acl/MessageTemplate.html) para poder discriminar los mensajes que recibimos del buzón:

```java
...
MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
ACLMessage msg = myAgent.receive(mt);
if (msg != null) { 
    // El mensaje coincide con la plantilla 
} else { 
    // No tenemos mensaje
    block();
}
...
```

## 2.4 Servicio de páginas amarillas

En cada plataforma de agentes Jade disponemos de un agente especializado, `DF`, que es el encargado de este servicio. Este servicio será necesario para que se puedan localizar agentes que proporcionan algún tipo de servicio, o más de uno, para que otros agentes puedan solicitarlos. Esto es necesario porque en el diseño de los agentes no podemos suponer el momento en que el agente de un tipo podrá estar disponible  o el número de ellos. 

![][paginasAmarillas]

Para poder trabajar con este servicio necesitamos un objeto de la clase [DFAgentDescription](https://jade.tilab.com/doc/api/jade/domain/FIPAAgentManagement/DFAgentDescription.html) para representar el agente que quiere darse de alta en el servicio, con su `AID`. El agente puede proporcionar uno o más servicios, para ello hay que utilizar una instancia de la clase [ServiceDescription](https://jade.tilab.com/doc/api/jade/domain/FIPAAgentManagement/ServiceDescription.html) y añadir la descripción del servicio. Por último la clase [DFService](https://jade.tilab.com/doc/api/jade/domain/DFService.html) nos proporciona el método `register(.)` y `deregister(.)` para dar de alta y eliminar un registro en el servicio de páginas amarillas.

En el ejemplo que vamos a desarrollar a continuación veremos los pasos necesarios para darse de alta y baja en el servicio de páginas amarillas. 

### Localizar un agente en el servicio de páginas amarillas

Para que los agentes puedan localizar algún otro agente, por medio del servicio que proporciona, en el servicio de páginas amarillas disponemos de dos posibilidades:

1. Crear una tarea cíclica que busque en el servicio los agentes que cumplan con el servicio deseado.
2. Suscribirse al servicio de páginas amarillas y que nos proporcione información cada vez que un agente deseado se registre o abandone el servicio de páginas amarillas. Este será el método más apropiado y que se explicará a continuación.

#### Suscripción en el servicio de páginas amarillas

Para ello crearemos una tarea que herede de la clase [DFSubscriber](https://jade.tilab.com/doc/api/jade/domain/DFSubscriber.html) he implementar los métodos abstractos y constructor que nos indica el entorno de desarrollo:

```java
class TareaSuscripcionDF extends DFSubscriber {
    public TareaSuscripcionDF(Agent a, DFAgentDescription template) {
        super(a, template);
    }

    @Override
    public void onRegister(DFAgentDescription dfad) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public void onDeregister(DFAgentDescription dfad) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }    
}
```

En el desarrollo de la práctica veremos la personalización de esta tarea.

## 2.5 Desarrollo de la práctica

En la práctica se diseñaran tres tipos de agentes que intercambiarán mensajes para resolver unos objetivos concretos Los agentes son:

1. [**Agente Consola**](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteConsola.md): Presentará una ventana asociada a cada uno de los agentes que le envíe mensajes que deban ser presentados en el sistema. Sus tareas serán:
	- *Recibir mensajes*: Tarea cíclica que espera un mensaje que se almacenará en una estructura para añadirlo a la ventana correspondiente del agente que lo envió.
	- *Presentar mensaje*: Será una tarea de una sola ejecución por cada mensaje que se quiera presentar en la ventana del agente que envió el mensaje. Si no está previamente creada se creará una nueva.

2. [**Agente Formulario**](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteFormulario.md): Tendrá una interfaz que le permitirá recoger un **Punto** y enviarlo a los **Agentes Operación** que conozca en el momento de envío. Sus tareas serán:
	- *Localizar agentes*: Se suscribirá al servicio de páginas amarillas para que sea notificado de los **Agentes Consola** y **Agentes Operación** presentes en la plataforma donde se aloja el **Agente Formulario**.
	- *Envío Operación*: Una tarea de un solo uso para enviar el **Punto** a los **Agentes Operación** conocidos.
	- *Envío Consola*: Tarea cíclica cada 10 segundos para enviar a un **Agente Consola** conocido los envíos de operación completados.

3. [**Agente Operación**](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteOperacion.md): Este agente realizará una operación matemática aleatoriamente de entre las que conozca. Las tareas que realiza son:
	- *Localizar agentes*: Se suscribirá al servicio de páginas amarillas para que sea notificado de los **Agentes Consola** presentes en la plataforma donde se aloja el **Agente Operación**.
	-  *Recepción operación*: Tarea cíclica para la recepción de los mensajes de operación que pueda recibir de los **Agentes Formulario**.
	- *Cálculo*: Tarea que se realiza una vez por cada operación solicitada y que aplicará una operación matemática aleatoria de las conocidas.
	- *Envío Consola*: Tarea cíclica cada 10 segundos para enviar a un **Agente Consola** conocido los envíos de cálculo completados.

### Inicio

Abrimos el [proyecto](https://gitlab.com/ssmmaa/guionsesion2/-/archive/v0.1/guionsesion2-v0.1.zip) en NetBeans y creamos los siguientes paquetes dentro del paquete `es.uja.ssmmaa.guionsesion2`: 

- `gui`: para las clases que representarán las interfaces necesarias.
- `util`: para las clases de apoyo.

Creamos en el paquete `es.uja.ssmmaa.guionsesion2` una interface con el nombre `Contantes` donde se almacenarán las constantes necesarias para el proyecto:

```java
public interface Constantes {
    public static final long REPETICION = 10000; // 10 segundos
    public static final int NO_ENCONTRADO = 0;
    public static final int PRIMERO = 0;
    public static final int SEGUNDO = 1;
    public static final int D100 = 100; // Representa un dado de 100
    
    public static final String TIPO_SERVICIO = "Agentes Servicio";
    public enum NombreServicio {
        OPERACION, CONSOLA;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Operacion {
        SUMA(25), RESTA(50), MULTIPLICACION(75), DIVISION(100);
        
        private int valor;

        private Operacion(int valor) {
            this.valor = valor;
        }
        
        /**
         * Devuelve una operación de las disponibles aleatoriamente
         * @return Operacion; una de las operaciones disponibles
         */
        public static Operacion getOperacion() {
            Random aleatorio = new Random();
            int tiradaDado = aleatorio.nextInt(D100);
            
            for( Operacion operacion : Operacion.values() ) {
                if( operacion.valor > tiradaDado )
                    return operacion;
            }
            
            return DIVISION;
        }
    }
    public static final Operacion[] OPERACIONES = Operacion.values();
}
```

De esta forma podremos retocar algunos elementos de configuración sin necesidad de recodificar nuestros agentes. Además ayuda a una legibilidad mayor del código. Esta es una norma que se deberá utilizar en el desarrollo de las prácticas de la asignatura.

### Paquete `util`

En este paquete se incluyen dos clases de apoyo para la resolución del ejercicio. Las clases son:

- `MensajeConsola`: Esta clase es utilizada por los agentes cuando quieren enviar un mensaje al `AgenteConsola`.
	- `private String nombreAgente`: Para almacenar la información relativa al agente que envía el mensaje
	- `private String contenido`: Para almacenar lo que se presentará en la consola.
	- Tiene definido un constructor para dar valores a las dos variables de instancia.
	- La clase tiene los métodos de acceso necesarios y el método `toString()` como utilidad para la depuración.

- `Punto2D`: Esta clase está como ejemplo para la definición de clase necesaria para ser utilizada como dato de nuestra solución. Solo está para ejemplificar la necesidad del diseño de datos para nuestro problema. Dependiendo de la complejidad del problema se definirán tantas clases como se estimen oportunas.
	- `private double x;` y `private double y;`: Son las dos variables que nos van a representar un punto del espacio 2D.
	- Tiene definidos dos constructores, uno para una inicialización por defecto y otro para asignar valores a las variables de instancia.
	- Tiene definidos los métodos de acceso y el método `toString()` como utilidad para la depuración.

### Paquete `gui`

Este paquete tiene las interfaces que serán utilizadas por los agentes y son:

- [`ConsolaJFrame`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/README.md#consolajframe) hereda de `JFrame`: Es la visualización de una consola asociada a un agente. El `AgenteConsola` las utiliza para asociarlas a los mensajes que recibe de los `AgenteFormulario` y `AgenteOperacion` que le envíen mensajes.
- [`FormularioJFrame`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/README.md#formulariojframe) hereda de `JFrame`: Es la interface asociada a cada `AgenteFormulario` presente en la plataforma. Permite recoger la información que luego será enviada a los AgenteOperación.
- [`OkCancelDialog`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/README.md#okcanceldialog) hereda de `JDialog`: Es el cuadro de diálogo que se mostrará cuando se intente finalizar un agente. 

Ahora se presentarán un poco más en detalle algunos elementos importantes del diseño de estas clases. Hay que recordar eliminar el método `main(.)` que nos cree el asistente y no tocar el código que sea generado por el asistente visual que nos proporciona NetBeans.

#### `ConsolaJFrame`

Como única variable necesitaremos el nombre del agente que se almacenará como un `String` para presentarlo en el título de la ventana. La ventana incluirá una `JTexArea` con una barra de desplazamiento vertical. Así podremos desplazarnos por los mensajes que el agente envíe. El resultado visual será parecido al que se muestra en la figura:

![][ConsolaJFrame]

Los métodos que definiremos serán:

- Obtener el `String` que representa el nombre del agente.
- Un método que permita al `AgenteConsola` añadir un mensaje al área de texto.
- Dar una implementación al método que responde al evento que cierre la ventana.

El código es:

```java
...
    /**
     * Creates new form Consola
     * @param nombreAgente
     */
    public ConsolaJFrame(String nombreAgente) {
        super(nombreAgente);
        
        initComponents();
        this.nombreAgente = nombreAgente;
    }

    public String getNombreAgente() {
        return nombreAgente;
    }
    
    public void presentarSalida (MensajeConsola mensaje) {
        if (isVisible() != true) {
            setVisible(true);
        }
        salida.append(mensaje.toString());
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {                                   
        // TODO add your handling code here:
        setVisible(false);
    }                                  
...
```

#### `FormularioJFrame`

Como variables de instancia de la clase necesitamos una referencia para del `AgenteFormulario` al que esté asociado y una referencia a la clase `OkCancelDialog` para el cuadro de diálogo de cancelación. Los elementos gráficos de la ventana son los siguientes:

- Tres `JLabel` para el texto que describe la funcionalidad de la interface.
- Dos `JTextField` para obtener la información necesaria que compondrá un objeto de la clase `Punto2D`.
- Un `JButton` para componer el objeto `Punto2D` y pasarlo al agente para que pueda crear la tarea de envío al los `AgenteOperacion` conocidos. 

Una diseño aproximado se puede ver en la siguiente imagen:

![][FormularioJFrame]

Los métodos que hay que definir son:

- Un método que permita activar o inactivar el botón de envío por parte del `AgenteFormulario`.
- El método de acción de botón para obtener el `Punto2D` y enviarlo al `AgenteFormulario`.
- El método que responde al cierre de la ventana para crear el cuadro de diálogo `OkCancelJDialog`.

El código es:

```java
...
    /**
     * Creates new form FormularioJFrame
     * @param myAgent
     */
    public FormularioJFrame(AgenteFormulario myAgent) {
        super (myAgent.getName());
        
        initComponents();
        this.myAgent = myAgent;
    }

    /**
     * Método para activar el botón de enviar si hay agentes operación
     * disponibles
     * @param activar
     */
    public void activarEnviar(boolean activar) {
        enviar.setEnabled(activar);
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {                                   
        // TODO add your handling code here:
        finalizacion = new OkCancelDialog(this, true, myAgent);
        finalizacion.setVisible(true);
    }                                  

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        Punto2D punto;
        
        punto = new Punto2D();
        
        punto.setX(Double.parseDouble(coorX.getText()));
        punto.setY(Double.parseDouble(coorY.getText()));
        
        myAgent.enviarPunto2D(punto);
    }      
...
```

#### `OkCancelDialog`

Cuando la interface asociada a un `AgenteFormulario` se cierra aparece esta ventana para confirmar la finalización del agente. Las variables de instancia que se necesitan son el `AgenteFormulario` y un valor que indica la acción que se ha adoptado. Los elementos gráficos que necesitamos son:

- Un `JLabel` para mostrar el nombre del `AgenteFormularo` que se desea finalizar.
- Dos `JButton` para recoger las dos acciones posibles para el usuario.

Un diseño aproximado se muestra en la siguiente imagen:

![][OkCancel]

Los métodos que debemos diseñar son los siguientes:

- El constructor para asegurarnos que cumple con la operatividad deseada.
- Los métodos de acción de los botones para que se comporten como se espera de ellos.
- Los métodos asociados con el cierre de la ventana para que se comporten igual que el botón de cancelación.

El código será:

```java
...
    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    /**
     * Creates new form OkCancelDialog
     * @param parent
     * @param modal
     * @param myAgent
     */
    public OkCancelDialog(java.awt.Frame parent, boolean modal, Agent myAgent) {
        super(parent, modal);
        initComponents();
        this.myAgent = myAgent;
        nombreAgente.setText("Agente: " + this.myAgent.getName());

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }
    
    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
        myAgent.doDelete();
        doClose(RET_OK);
    }                                        

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        doClose(RET_CANCEL);
    }                                            

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {                             
        doClose(RET_CANCEL);
    }                            
    
    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }
...
```

### Paquete `agententes`

En el proyecto, desde donde parte esta práctica, tiene este paquete con un `AgentePlantilla` que deberemos duplicar para cada uno de los agentes que son necesarios en la práctica. En nuestro caso, los agentes para la práctica son:

- [`AgenteConsola`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteConsola.md)
- [`AgenteFormulario`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteFormulario.md)
- [`AgenteOperacion`](https://gitlab.com/ssmmaa/guionsesion2/-/blob/master/AgenteOperacion.md)

## 2.6 Modificaciones iniciales propuestas

Como primer paso crear el paquete `tareas` desde el paquete inicial `es.uja.ssmmaa.guionsesion2` para incluir las tareas de todos los agentes. Las tareas serán eliminadas como clases internas. Hay que adaptar el diseño de los agentes y tareas para que proporcione la misma funcionalidad que tienen.

## 2.7 Práctica a realizar

Nos descargamos el [proyecto](https://gitlab.com/ssmmaa/guionsesion2/-/archive/v1.1/guionsesion2-v1.1.zip) para empezar realizado las modificaciones que se proponen en el punto anterior. Una vez que se han finalizado las modificaciones hay que añadir:

- Definir un `AgenteMonitor` que presentará en una ventana los agentes presentes en la plataforma de los diferentes tipos, es decir, presentará una lista para los `AgenteFormulario`, `AgenteConsola`, `AgenteOperación` presentes en la plataforma. Para ello hay que modificar el `AgenteFormulario` y la interface `Constantes` para establecer los parámetros necesarios para el registro de `AgenteFormulario` en el servicio de páginas amarillas. Además debe llevar una contabilidad de las operaciones solicitaras por cada `AgenteFormulario` y las completadas, de cada tipo, por los `AgenteOperacion`. Hay que diseñar las tareas que sean necesarias para este agente pueda completar los objetivos detallados.
- Definir una clase utilidad `OperacionCompletada` para un `AgenteOperacion` pueda comunicar al `AgenteMonitor` el tipo de operación que ha realizado.
- Crear el contenido de los mensajes mediante `json` para que el intercambio de mensajes entre los agentes sea homogéneo y así permitir la comunicación de todos los agentes desarrollados para esta práctica por cualquier alumno.
- Eliminar todos los mensajes a consola para la depuración de los agentes y presentarlos en una ventana asociada a cada agente para que el usuario pueda revisarlos.


## 2.8 Pruebas de ejecución

Para la práctica hay que comprobar que la ejecución de los agentes debe superar las pruebas siguientes:

- Todas las excepciones, por los métodos utilizados en los agentes, *lanzadas* hay que *capturarlas* y tomar las decisiones apropiadas.
- *Capturar* el resto de excepciones, las *no capturadas*, y adoptar las acciones apropiadas. Para probar que se está haciendo bien, permitir la división por `0` y así comprobar que se está tratando correctamente esta excepción.
- Utilizar la utilidad del agente `Dummy` para enviar mensajes con un contenido de mensaje incorrecto a cualquier otro agente desarrollado para la práctica y comprobar que no provoca un fallo catastrófico y que son identificados correctamente. Adoptando las acciones apropiadas para el buen funcionamiento del agente.
- Para todas las ventanas gráficas, comprobar que tienen una correcta *usuabilidad* y que el usuario no puede realizar operaciones no deseadas.


[cicloVida]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/CicloVida.jpg
[arquitecturaJade]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/arquitectura.jpg
[comunicacionAgentes]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/ComunicacionAgentes.jpg
[paginasAmarillas]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/PaginasAmarillas.jpg
[ConsolaJFrame]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/ConsolaJFrame.png
[FormularioJFrame]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/FormularioJFrame.png
[OkCancel]: https://gitlab.com/ssmmaa/guionsesion2/-/raw/master/imagenes/OkCancel.png 


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0NDUwMDgzNDAsNDY4NjU2ODI1LC0xMz
UzMTU2ODA1LDE0MzQwODM2NTEsLTE2OTk1NTkzNzgsODY4MzQ0
ODEzLDcwNzc0NjIyMSw0OTEzMjA4MzUsLTYyMDcxNTIxOCwtMT
cyOTg5NTk2Nl19
-->